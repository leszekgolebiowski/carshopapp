<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Order Confirmation</title>
</head>
<body>

There is a new order!
<br><br>
<u>Ordered part</u>
<br>
<b>${order.partName}</b>
<br><br>
<u>Adress</u>
<br>
${order.firstName}
<br>
${order.lastName}
<br>
${order.street}
<br>
${order.postalCode}
<br>
${order.town}
<br><br>
Discount Code: ${order.discountCode}

</body>
</html>
