<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Order Form</title>
    <style>
        .error {color:red}
    </style>
</head>
<body>

<form:form action="processForm" modelAttribute="order">

    Part to order: <form:input path="partName"></form:input>
    <form:errors path="partName" cssClass="error"></form:errors>
    <br><br>

    First name: <form:input path="firstName"></form:input>
    <form:errors path="firstName" cssClass="error"></form:errors>
    <br><br>

    Last name: <form:input path="lastName"></form:input>
    <form:errors path="lastName" cssClass="error"></form:errors>
    <br><br>

    Street: <form:input path="street"></form:input>
    <form:errors path="street" cssClass="error"></form:errors>
    <br><br>

    Postal Code: <form:input path="postalCode"></form:input>
    <form:errors path="postalCode" cssClass="error"></form:errors>
    <br><br>

    Town: <form:input path="town"></form:input>
    <form:errors path="town" cssClass="error"></form:errors>
    <br><br>

    Discount code: <form:input path="discountCode"></form:input>
    <form:errors path="discountCode" cssClass="error"></form:errors>

    <input type="submit" value="Order">

</form:form>

</body>
</html>
