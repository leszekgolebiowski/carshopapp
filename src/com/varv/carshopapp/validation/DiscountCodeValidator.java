package com.varv.carshopapp.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DiscountCodeValidator implements ConstraintValidator<DiscountCode, String> {

    private String prefix;

    public void initialize(DiscountCode constraint) {
        prefix = constraint.value();
    }

    public boolean isValid(String theCode, ConstraintValidatorContext context) {

        boolean result;

        if (theCode != null) {
            result = theCode.startsWith(prefix);
        } else {
            result = true;
        }

        return result;
    }
}
