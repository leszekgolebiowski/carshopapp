package com.varv.carshopapp.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = DiscountCodeValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DiscountCode {

    public String value() default "MXP";

    public String message() default "This is not a valid discount code";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default {};

}
