package com.varv.carshopapp;

import com.varv.carshopapp.validation.DiscountCode;

import javax.validation.constraints.*;

public class Order {

    @NotNull(message = "is required")
    private String partName;

    @NotNull(message = "is required")
    @Size(min = 3, message = "too short")
    private String firstName;

    @NotNull(message = "is required")
    @Size(min = 3, message = "too short")
    private String lastName;

    @NotNull(message = "is required")
    @Size(min = 3, message = "too short")
    private String street;

    @NotNull(message = "is required")
    @Size(min = 3, message = "too short")
    private String town;

    @NotNull(message = "is required")
    @Pattern(regexp = "^[a-zA-Z0-9]{5}", message = "only 5 chars/digits")
    private String postalCode;

    @DiscountCode
    private String discountCode;

    public Order() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getDiscountCode() {
        return discountCode;

    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }
}
